/*
 * The MIT License
 * Copyright (c) 2018.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.network.mappers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tegiffel.oscar.shoutr.infrastructure.error.ApiError;

import java.io.IOException;

/**
 * Created by Oscar on 01/01/2018.
 */

public class ApiErrorJsonMapper {

    // The object jackson json mapper.
    private ObjectMapper objectMapper;

    /**
     * Constructor of ApiErrorJsonMapper.
     */
    public ApiErrorJsonMapper() {
        this.setupObjectMapper();
    }

    /**
     * Setup the object jackson json mapper.
     */
    private void setupObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        this.objectMapper = objectMapper;
    }

    /**
     * Transform the given api json response to an ApiError object.
     *
     * @param apiErrorJsonString The api error json string.
     * @return An ApiError object if successfully transformed.
     * @throws IOException
     */
    public ApiError transformApiError(String apiErrorJsonString) throws IOException {
        return objectMapper.readValue(apiErrorJsonString, ApiError.class);
    }
}