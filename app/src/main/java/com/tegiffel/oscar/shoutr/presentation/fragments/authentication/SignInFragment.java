/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.presentation.fragments.authentication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tegiffel.oscar.shoutr.R;
import com.tegiffel.oscar.shoutr.presentation.activities.ChatRoomsActivity;
import com.tegiffel.oscar.shoutr.presentation.fragments.BaseFragment;
import com.tegiffel.oscar.shoutr.presentation.fragments.authentication.listeners.OnSignInSuccessfulListener;

/**
 * Created by Oscar on 23/12/2017.
 */

public class SignInFragment extends BaseFragment {

    public static final String TAG = "SignInFragment";

    // The sign in button.
    private Button signInButton;

    // The username edit text.
    private EditText usernameEditText;

    // The password password edit text.
    private EditText passwordEditText;


    private OnSignInSuccessfulListener onSignInSuccessfulListener;

    /*
    Life cycle methods
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.onSignInSuccessfulListener = (OnSignInSuccessfulListener) context;
        Log.d("Testing", "Testing");
    }

    /**
     * The system calls this method when creating the fragment. We should initialize essential
     * components of the fragment that we want to retain when the fragment is paused
     * or stopped, then resumed.
     *
     * @param savedInstanceState The saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.boot();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.setupSignInButton(view);
    }

    /**
     * The default boot method for the fragment. This method will be called before
     * the fragment completed loading its view. (Pre-)Setting variables and
     * such will be done here.
     */
    private void boot() {
        // Empty
    }

    /**
     * Setup the sign in button using the parent's view.
     *
     * @param parent The parent view.
     */
    private void setupSignInButton(View parent) {
        Button signInButton = parent.findViewById(R.id.button_sign_in);
        signInButton.setOnClickListener(new SignInButtonOnClickListener());
        this.signInButton = signInButton;
    }

    public void signIn() {
        Log.d("Okay", "Sign in please");
    }

    private class SignInButtonOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
//            Intent myIntent = new Intent(getActivity(), ChatRoomsActivity.class);
//            startActivity(myIntent);
        }
    }


    public void showError(String message)
    {
        Toast.makeText(
                this.getContext(),
                message,
                Toast.LENGTH_LONG
        ).show();
    }


    /**
     * The user was signed in successfully.
     */
    public void onSignInSuccess()
    {
        this.onSignInSuccessfulListener.onSignInSuccessful();
    }
}
