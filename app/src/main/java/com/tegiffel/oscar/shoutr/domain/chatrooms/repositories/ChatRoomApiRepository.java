/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.domain.chatrooms.repositories;

import com.tegiffel.oscar.shoutr.data.repositories.ApiRepository;
import com.tegiffel.oscar.shoutr.data.repositories.RepositoryCompletionHandler;
import com.tegiffel.oscar.shoutr.infrastructure.providers.AccessTokenProvider;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;


public class ChatRoomApiRepository extends ApiRepository {

    public void getAll(final RepositoryCompletionHandler completionHandler)
    {
        // @See getChatRoomById();
    }

    // Method call the API which will return back into the showChatRoom method.
    public void getChatRoomById(int id, final RepositoryCompletionHandler completionHandler)
    {
        Request request = new Request.Builder()
                .url(ApiRepository.URI_BASE + "chatrooms/" + id)
                .header("Accept", ApiRepository.JSON.toString())
                .header("Authorization", "Bearer " + AccessTokenProvider.getInstance().getAccessToken().getValue())
                .build();

        this.client.newCall(request)
                .enqueue(new Callback() {

                    @Override
                    public void onFailure(Call call, IOException error)
                    {
                        completionHandler.onError(error);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException
                    {
                        if (!response.isSuccessful())
                        {
                            throw new IOException("Unexpected code " + response);
                        }

                        completionHandler.onSuccess(response.body().string());
                    }
                });
    }

}
