/*
 * The MIT License
 * Copyright (c) 2018.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.error;

import android.content.Context;
import android.content.res.Resources;

import com.tegiffel.oscar.shoutr.R;
import com.tegiffel.oscar.shoutr.infrastructure.Shoutr;

/**
 * Created by Oscar on 01/01/2018.
 */

public class Error extends Throwable {

    // The error code.
    private final int code;

    // The error message.
    private final String message;

    /**
     * The designated initializer to initialize the error with code and message.
     *
     * @param code    The error code.
     * @param message The error message.
     */
    public Error(int code, String message) {
        super(message);

        this.code = code;
        this.message = message;
    }

    /**
     * Initialize the error with an error code.
     * It will try to find the associated error message using the error code.
     *
     * @param code The error code.
     */
    public Error(int code) {
        this(code, Error.getMessageByCode(code));
    }

    /**
     * The error code.
     *
     * @return The error code.
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Get the error message.
     *
     * @return The error message.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Get the message associated with the error code.
     * A fall back message will be returned if there's no message found associated
     * with the error code.
     *
     * @param code The error code.
     * @return The message associated with the error code, or a fallback message.
     */
    private static String getMessageByCode(int code) {
        Context context = Shoutr.getContext();
        Resources resources = context.getResources();

        // Try to get the message by code dynamically.
        try {
            return resources.getString(
                    resources.getIdentifier(
                            "error.message.with_code." + code,
                            "string",
                            context.getPackageName()
                    )
            );
        } catch (Resources.NotFoundException exception) {
            // Return fall back string, if the dynamically searched string identifier is not found.
            return resources.getString(R.string.error_message_with_code_unknown, code);
        }
    }

    /**
     * Intended only for debugging.
     * <p/>
     * Here, the contents of every field are placed into the result, with
     * one field per line.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String lineSeparator = System.getProperty("line.separator");

        result.append(this.getClass().getName() + " Object {" + lineSeparator);
        result.append(" Code: " + this.getCode() + lineSeparator);
        result.append(" Message: " + this.getMessage() + lineSeparator);
        result.append("}");

        return result.toString();
    }
}