/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.presentation.fragments.settings;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tegiffel.oscar.shoutr.R;
import com.tegiffel.oscar.shoutr.domain.chatrooms.repositories.ChatRoomApiRepository;
import com.tegiffel.oscar.shoutr.data.repositories.RepositoryCompletionHandler;
import com.tegiffel.oscar.shoutr.domain.chatrooms.ChatRoom;
import com.tegiffel.oscar.shoutr.presentation.fragments.BaseFragment;
import com.tegiffel.oscar.shoutr.transformers.ChatRoomTransformer;

/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsFragment extends BaseFragment {

    @Override
    public void onStart()
    {
        super.onStart();

        Log.d("Oscar", "onStart: Hello world!");

        final ChatRoomApiRepository chatRoomApiRepository = new ChatRoomApiRepository();
        chatRoomApiRepository.getChatRoomById(2, new RepositoryCompletionHandler() {
            @Override
            public void onError(Throwable error) {

            }

            @Override
            public void onSuccess(String json) {
                // Raw json string -> ChatRoom.java object.
                // Transformer.transform(json) -> ChatRoom.

                ChatRoomTransformer chatRoomTransformer = new ChatRoomTransformer();
                ChatRoom chatRoom = chatRoomTransformer.transform(json);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

}
