/*
 * The MIT License
 * Copyright (c) 2018.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.domain.authentication.services;

import android.content.Context;

import com.tegiffel.oscar.shoutr.domain.authentication.CredentialsStore;
import com.tegiffel.oscar.shoutr.domain.authentication.SignedIn;
import com.tegiffel.oscar.shoutr.domain.authentication.requests.SignInRequest;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by Oscar on 01/01/2018.
 */

public class SignInService {

    // The store credentials.
    private CredentialsStore storeCredentials;

    /**
     * Initialize the sign in service with the android context.
     *
     * @param context The context.
     */
    public SignInService(Context context)
    {
        this.storeCredentials = new CredentialsStore(context);
    }

    /**
     * Sign in with username and password.
     *
     * @return An observable containing the a signed in object.
     */
    public Observable<SignedIn> signIn(SignInRequest request)
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("email", request.getEmail());
        parameters.put("password", request.getPassword());

        // Sign in with username and password using grant type.
        return Api.getSharedInstance().signIn(parameters)
                .doOnNext(new Action1<SignedIn>() {

                    @Override
                    public void call(SignedIn signedIn)
                    {
                        SignInService.this.storeCredentials.storeAccessToken(signedIn.getAccessToken());
                    }

                });
    }
}
