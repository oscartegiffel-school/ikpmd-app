/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure;

import android.app.Application;
import android.content.Context;

import com.tegiffel.oscar.shoutr.domain.authentication.AccessToken;
import com.tegiffel.oscar.shoutr.domain.authentication.CredentialsStore;
import com.tegiffel.oscar.shoutr.infrastructure.providers.AccessTokenProvider;
import com.tegiffel.oscar.shoutr.infrastructure.providers.AccessTokenProviderInterface;

public class Shoutr extends Application {

    // The context of the application.
    private static Context context;

    /**
     * Perform the top and first most application startup logic
     * here that should happen only once for the entire lifecycle of the application.
     */
    @Override
    public void onCreate()
    {
        super.onCreate();

        this.boot();
    }

    /**
     * Boot the
     */
    private void boot()
    {
        context = this.getApplicationContext();

        AccessTokenProvider.setAccessTokenProviderInstance(
                () -> {
                    CredentialsStore credentialsStore =
                            new CredentialsStore(
                                    Shoutr.this.getApplicationContext()
                            );
                    return credentialsStore.getAccessToken();
                });
    }

    /**
     * Get the application context.
     *
     * @return The application context.
     */
    public static Context getContext()
    {
        return context;
    }

}
