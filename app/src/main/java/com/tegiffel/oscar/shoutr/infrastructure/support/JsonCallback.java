/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.support;

import com.tegiffel.oscar.shoutr.infrastructure.error.ApiError;
import com.tegiffel.oscar.shoutr.infrastructure.error.Error;
import com.tegiffel.oscar.shoutr.infrastructure.error.ErrorCode;
import com.tegiffel.oscar.shoutr.infrastructure.network.mappers.ApiErrorJsonMapper;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Oscar on 31/12/2017.
 */

public abstract class JsonCallback implements Callback {

    @Override
    public void onFailure(Call call, IOException exception)
    {
        if (!call.isCanceled())
        {
            Error error = new Error(ErrorCode.UNKNOWN_ERROR);
            this.onFailure(call, error);
        }
    }

    public void onResponse(Call call, Response response) throws IOException
    {
        String jsonString = response.body().string();

        if (!isResponseBetweenSuccessfulHttpCodes(response))
        {
            ApiErrorJsonMapper apiErrorJsonMapper = new ApiErrorJsonMapper();
            ApiError apiError = apiErrorJsonMapper.transformApiError(jsonString);

            this.onFailure(call, apiError);
        }
        else
        {
            this.onJsonResponse(call, jsonString);
        }
    }

    /**
     * Checks if the response is between successful http codes.
     *
     * @param response The response to check.
     * @return true if it is between successful http codes, otherwise false.
     */
    private Boolean isResponseBetweenSuccessfulHttpCodes(Response response)
    {
        return response.code() >= 200 && response.code() < 300;
    }

    /**
     * Called whenever a json response is received.
     *
     * @param call The original call.
     * @param json The json string response.
     * @throws IOException
     */
    public abstract void onJsonResponse(Call call, String json) throws IOException;

    /**
     * Called whenever an error was found.
     *
     * @param call  The original call.
     * @param error The error that was found.
     */
    public abstract void onFailure(Call call, Error error);
}
