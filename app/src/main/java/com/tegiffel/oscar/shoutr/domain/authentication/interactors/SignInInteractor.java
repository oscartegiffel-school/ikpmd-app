/*
 * The MIT License
 * Copyright (c) 2018.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.domain.authentication.interactors;

import com.tegiffel.oscar.shoutr.domain.authentication.requests.SignInRequest;

/**
 * Created by Oscar on 01/01/2018.
 */

public class SignInInteractor {


    // The request containing data to sign in.
    private SignInRequest request;

    // The sign in service.
    private final SignInService signInService;

    /**
     * Designated initializer for the interactor to sign in.
     *
     * @param subscribeOnScheduler The subscribe on scheduler.
     * @param observeOnScheduler   The observe on scheduler.
     * @param signInService        The sign in service.
     */
    public SignInInteractor(Scheduler subscribeOnScheduler,
                            Scheduler observeOnScheduler,
                            SignInService signInService)
    {
        super(subscribeOnScheduler, observeOnScheduler);

        this.signInService = signInService;
    }

    /**
     * Execute to sign in.
     *
     * @param request    The request to sign in.
     * @param subscriber The subscriber.
     */
    public void execute(SignInRequest request, Subscriber subscriber)
    {
        this.request = request;

        super.execute(subscriber);
    }

    /**
     * Builds an {@link Observable} which will
     * be used when executing the current {@link Interactor}.
     */
    @Override
    protected Observable buildObservable()
    {
        return this.signInService.signIn(request);
    }

}
