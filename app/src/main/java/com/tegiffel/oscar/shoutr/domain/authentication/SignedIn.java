/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.domain.authentication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by Oscar on 30/12/2017.
 */

@JsonRootName("data")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignedIn {

    private AccessToken accessToken;

    public SignedIn(AccessToken accessToken) {
        this.accessToken = accessToken;
    }

    @JsonCreator
    public static SignedIn fromJson(
            @JsonProperty("access_token") String accessToken) {
        return new SignedIn(new AccessToken(accessToken));
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    /**
     * Intended only for debugging.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String lineSeparator = System.getProperty("line.separator");

        result.append(this.getClass().getName() + " Object {" + lineSeparator);
        result.append(" AccessToken: " + this.accessToken.toString() + lineSeparator);
        result.append("}");

        return result.toString();
    }

}
