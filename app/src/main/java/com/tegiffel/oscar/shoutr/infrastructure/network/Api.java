/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.network;

import android.util.Log;

import com.tegiffel.oscar.shoutr.domain.authentication.SignedIn;
import com.tegiffel.oscar.shoutr.infrastructure.error.Error;
import com.tegiffel.oscar.shoutr.infrastructure.network.mappers.SignedInJsonMapper;
import com.tegiffel.oscar.shoutr.infrastructure.support.JsonCallback;
import com.tegiffel.oscar.shoutr.infrastructure.support.observables.ApiRequestObservable;

import org.reactivestreams.Subscriber;

import java.io.IOException;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Oscar on 30/12/2017.
 */

public class Api {

    // The singleton shared instance.
    private static Api instance = null;
    // The http client.
    private final OkHttpClient client = new OkHttpClient();


    /**
     * To return a singleton
     */
    public static Api getSharedInstance() {
        if (instance == null) {
            synchronized (Api.class) {
                if (instance == null) {
                    instance = new Api();
                }
            }
        }
        return instance;
    }

    /**
     * Request sign in.
     *
     * @param parameters The parameters to send with the request.
     * @return An Observable containing {@link SignedIn} object if successfully transformed.
     */
    public Observable<SignedIn> signIn(final Map<String, String> parameters) {
        return Observable.create(new ApiRequestObservable<SignedIn>() {

            @Override
            public Request buildRequest() {
                HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.API_URL + "/oauth/token").newBuilder();
                String url = urlBuilder.build().toString();

                RequestBody formBody = new FormBody.Builder()
                        .add("grant_type", "password")
                        .add("client_id", "")
                        .add("email", parameters.get("email"))
                        .add("password", parameters.get("password"))
                        .build();

                return new Request.Builder()
                        .url(url)
                        .addHeader("Content-Type", Constants.CONTENT_TYPE)
                        .addHeader("Accept", Constants.CONTENT_TYPE)
                        .post(formBody)
                        .build();
            }

            @Override
            public void execute(Request request, Subscriber<? super SignedIn> subscriber) {
                Api.this.client.newCall(request).enqueue(new JsonCallback() {

                    @Override
                    public void onJsonResponse(Call call, String json) throws IOException {

                        Log.d("Shoutr", "Response sign in: " + json);

                        SignedInJsonMapper signedInJsonMapper = new SignedInJsonMapper();
                        SignedIn signedIn = signedInJsonMapper.transformSignedIn(json);

                        subscriber.onNext(signedIn);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onFailure(Call call, Error error) {
                        subscriber.onError(error);
                    }

                });
            }
        });
    }


}
