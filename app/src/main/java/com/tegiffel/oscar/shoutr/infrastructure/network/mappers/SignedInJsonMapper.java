/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.network.mappers;

/**
 * Created by Oscar on 31/12/2017.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tegiffel.oscar.shoutr.domain.authentication.SignedIn;

import java.io.IOException;

/**
 * SignedInJsonMapper
 * <p/>
 * Created by Thomas Neuteboom on 11-02-16.
 * Copyright 2016 MoneyMonk B.V. All rights reserved.
 */
public class SignedInJsonMapper {

    private ObjectMapper objectMapper;

    public SignedInJsonMapper() {
        this.setupObjectMapper();
    }

    private void setupObjectMapper() {
        this.objectMapper = new ObjectMapper();
    }

    public SignedIn transformSignedIn(String signedInJsonString) throws IOException {
        return objectMapper.readValue(signedInJsonString, SignedIn.class);
    }

}
