/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.domain.authentication;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

public class CredentialsStore {

    // The access token key.
    private static String ACCESS_TOKEN_KEY = "access_token";

    // The secure preferences.
    private final SharedPreferences sharedPreferences;

    /**
     * Initialize the credentials store with context.
     * The store credentials provides a way to save and clear the access token.
     *
     * @param context The context.
     */
    public CredentialsStore(Context context)
    {
        this.sharedPreferences
                = context.getSharedPreferences(
                "com.tegiffel.oscar.shoutr",
                Context.MODE_PRIVATE
        );
    }

    /**
     * Read the value associated with the given key of this value object from
     * the secure shared preferences.
     *
     * @return The value associated with the given key.
     */
    @Nullable
    public AccessToken getAccessToken()
    {
        String value = this.sharedPreferences
                .getString(CredentialsStore.ACCESS_TOKEN_KEY, null);

        if (value == null)
        {
            return null;
        }

        return new AccessToken(value);
    }

    /**
     * Store the value to the secure shared preferences.
     *
     * @return Boolean that determines if it was successfully stored.
     */
    public boolean storeAccessToken(AccessToken accessToken)
    {
        return this.sharedPreferences.edit()
                .putString(CredentialsStore.ACCESS_TOKEN_KEY,
                           accessToken.getValue()
                ).commit();
    }

    /**
     * Delete value from secure shared preferences.
     *
     * @return Boolean that determines if it was successfully deleted.
     */
    public boolean clear()
    {
        return this.sharedPreferences.edit()
                .remove(CredentialsStore.ACCESS_TOKEN_KEY)
                .commit();
    }

}
