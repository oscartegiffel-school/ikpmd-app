/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.support.observables;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.tegiffel.oscar.shoutr.infrastructure.Shoutr;
import com.tegiffel.oscar.shoutr.infrastructure.error.Error;
import com.tegiffel.oscar.shoutr.infrastructure.error.ErrorCode;

import org.reactivestreams.Subscriber;

import io.reactivex.Observable;
import okhttp3.Request;

/**
 * Created by Oscar on 30/12/2017.
 */

//public abstract class ApiRequestObservable<T> implements Observable.onSubscribe<T> {
public abstract class ApiRequestObservable<T> extends Observable.onSubscribe<T> {

    /**
     * Build the request.
     *
     * @return The built request.
     */
    public abstract Request buildRequest();

    /**
     * Execute the request.
     *
     * @param request    The request that was built.
     * @param subscriber The subscriber.
     */
    public abstract void execute(Request request, final Subscriber<? super T> subscriber);

    /**
     * Will be emitted by subscribing.
     *
     * @param subscriber The subscriber.
     */
    @Override
    public void call(final Subscriber<? super T> subscriber) {
        // Before we emit this call, check if we can.
        if (this.canProceedToExecute(subscriber)) {
            this.execute(this.buildRequest(), subscriber);
        }
    }

    /**
     * Determines if the call can proceed to execute.
     *
     * @param subscriber The subscriber.
     * @return Determination of whether the call can proceed to execute.
     */
    public boolean canProceedToExecute(final Subscriber<? super T> subscriber) {
        if (!this.hasInternetConnection()) {
            Error error = new Error(ErrorCode.NO_INTERNET_CONNECTION);
            subscriber.onError(error);
            return false;
        }

        return true;
    }

    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean hasInternetConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) Shoutr.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }

}
