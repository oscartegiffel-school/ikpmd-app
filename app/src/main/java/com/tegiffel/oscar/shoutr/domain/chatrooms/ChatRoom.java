/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.domain.chatrooms;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tegiffel.oscar.shoutr.domain.messages.Message;

import java.util.ArrayList;

/**
 * Created by Oscar on 01/12/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatRoom {

    private int id;

    private String name;

    private ArrayList<Message> messageArrayList;

    @JsonCreator
    public ChatRoom(
            @JsonProperty("id") int id,
            @JsonProperty("name") String name,
            @Nullable @JsonProperty("messages") ArrayList<Message> messageArrayList)
    {
        this.id = id;
        this.name = name;
        this.messageArrayList = messageArrayList;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public ArrayList<Message> getMessageArrayList()
    {
        return messageArrayList;
    }

    @Override
    public String toString()
    {
        return "ChatRoom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", messageArrayList=" + messageArrayList +
                '}';
    }
}
