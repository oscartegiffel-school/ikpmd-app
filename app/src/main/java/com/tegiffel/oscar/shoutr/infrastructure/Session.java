/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure;

import com.tegiffel.oscar.shoutr.infrastructure.providers.AccessTokenProvider;

public class Session {

    /**
     * SingletonHolder is loaded on the first execution of Singleton.get()
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder {

        private static final Session INSTANCE = new Session();
    }

    // Private constructor prevents instantiation from other classes
    private Session()
    {
        // Empty.
    }

    /**
     * Get the singleton instance of the application context.
     *
     * @return The application context.
     */
    public static Session get()
    {
        return SingletonHolder.INSTANCE;
    }

    /**
     * Determines if the session is expired.
     *
     * @return Boolean that determines if the session is expired.
     */
    public boolean isExpired()
    {
        return AccessTokenProvider.getInstance().getAccessToken() == null;
    }

}
