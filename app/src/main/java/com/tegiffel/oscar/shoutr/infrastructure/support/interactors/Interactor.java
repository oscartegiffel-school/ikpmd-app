/*
 * The MIT License
 * Copyright (c) 2018.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.infrastructure.support.interactors;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

/**
 * Created by Oscar on 01/01/2018.
 */

public abstract class Interactor {

    // The subscribe on scheduler.
    protected final Scheduler subscribeOnScheduler;

    // The observe on scheduler.
    protected final Scheduler observeOnScheduler;

    // The current subscription.
//    protected Subscription subscription = Disposable.empty();

    /**
     * Initialize the interactor.
     *
     * @param subscribeOnScheduler The subscribe on scheduler.
     * @param observeOnScheduler   The observe on scheduler.
     */
    public Interactor(Scheduler subscribeOnScheduler, Scheduler observeOnScheduler)
    {
        this.subscribeOnScheduler = subscribeOnScheduler;
        this.observeOnScheduler = observeOnScheduler;
    }

    /**
     * Builds an {@link Observable} which will
     * be used when executing the current {@link Interactor}.
     */
    protected abstract Observable buildObservable();

    /**
     * Execute the interactor.
     *
     * @param subscriber The subscriber.
     */
    @SuppressWarnings("unchecked")
    protected void execute(Subscriber subscriber)
    {
        this.subscription = this.buildObservable()
                .subscribeOn(subscribeOnScheduler)
                .observeOn(observeOnScheduler)
                .subscribe(subscriber);
    }

    /**
     * Unsubscribes from current {@link Subscription}.
     */
    public void unsubscribe()
    {
        if (!this.subscription.isUnsubscribed())
        {
            this.subscription.unsubscribe();
        }
    }
}
