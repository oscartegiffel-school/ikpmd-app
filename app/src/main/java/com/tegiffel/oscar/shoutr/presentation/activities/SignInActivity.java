/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.presentation.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.WindowManager;

import com.tegiffel.oscar.shoutr.R;
import com.tegiffel.oscar.shoutr.presentation.fragments.authentication.SignInFragment;
import com.tegiffel.oscar.shoutr.presentation.fragments.authentication.listeners.OnSignInSuccessfulListener;


public class SignInActivity extends BaseActivity implements OnSignInSuccessfulListener {

    /**
     * Perform basic application startup logic here that should happen only once for
     * the entire life of the activity.
     *
     * @param savedInstanceState Re-initialized activity bundle or null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.boot();


        Fragment fragment = new SignInFragment();

        this.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment, SignInFragment.TAG)
                .commit();

    }

    /**
     * Boot the activity.
     */
    private void boot() {
        this.setContentView(R.layout.activity_sign_in);

        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.color_primary));
    }

    @Override
    public void onSignInSuccessful() {
        Intent intent = new Intent(this, ChatRoomsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);
        this.finishAffinity();
    }
}
