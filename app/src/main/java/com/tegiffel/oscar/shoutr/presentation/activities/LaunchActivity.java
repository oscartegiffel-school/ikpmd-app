/*
 * The MIT License
 * Copyright (c) 2017.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @link https://gitlab.com/oscarteg/ikpmd
 */

package com.tegiffel.oscar.shoutr.presentation.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.tegiffel.oscar.shoutr.infrastructure.Session;

public class LaunchActivity extends AppCompatActivity {

    /**
     * Perform basic application startup logic here that should happen only once for
     * the entire life of the activity.
     *
     * @param savedInstanceState The saved instance state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Determine which activity to load based on existing access token here.
        this.startFirstActivity();
    }

    /**
     * Start the application's first activity.
     */
    private void startFirstActivity()
    {
        Intent intent = this.getTheActivityToStart();
        this.startActivity(intent);
        this.finish();
    }

    /**
     * Get the activity to start based on whether the user is signed in.
     *
     * @return The first activity to start the application with.
     */
    private Intent getTheActivityToStart()
    {
        if (Session.get().isExpired())
        {
            return new Intent(this, SignInActivity.class);
        }

        return new Intent(this, ChatRoomsActivity.class);
    }

}
